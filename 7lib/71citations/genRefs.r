# This script generates the package references of the loaded packages and a vector of listed packages.

library(bibtex)
library("wrapr")  # get qc() definition
plist_v <- (.packages())
plist_v <- c(plist_v, qc(
    base,
    data.table,
    readr,
    tidyverse,
    sp,
    raster, 
    gdistance,
    lattice,
    ggplot2,
    r2d2,
    rCharts,
    highcharter,
    archdata,
    vioplot,
    random,
    RSQLite,
    sqldf,
    bibtex,
    knitr,
    rmarkdown,
    readr,
    magrittr))
plist_v <- unique(plist_v)
write.bib(plist_v, file = '7lib/71citations/lit_packages')


