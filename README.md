# Winter School `KlassifikatoR`

<!-- 
cd /home/fon/daten/ereignis/2020/klassifikatoR
pandoc -s -f   markdown -t  html   README.md   -o   flyer_klassifikatoR.html
pandoc -s -f   markdown -t  latex   README.md   -o   flyer_klassifikatoR.pdf  --latex-engine=xelatex
pandoc -s -f   markdown -t  odt   README.md   -o   flyer_klassifikatoR.odt
 --> 

This tutorial is a result of the winter school "KlassifikatoR" that took place 09th - 12th March 2020. It was organised by Oliver Nakoinz at the Christian-Albrechts-University in Kiel and the Johanna Mestorf Academy and funded by Perle. 

Methods for classfication play a central role in archaeology. During the winter school basic knowledge of different cluster analyses was taught by Oliver Nakoinz, Sophie Schmidt, Georg Roth and Martin Hinz (presentations can be found in the folder `./6praes`). The participants also learned git and RMarkdown to be able to develop teaching materials together, which can be shared further. This long-term aim of the winter school has been achieved with the release of this tutorial. In the folder `./5tutorials` an *tutorial_complete_ENG.html* can be found which offers an introduction into classification in general, distance measures, hierarchical clustering, k-means clustering, hdbscan clustering and the silhouette validation method.  We encourage anyone interested to take a look at the underlying Rmarkdown file as well. 

The tutorial aims at people somewhat familiar with the scripting language R, but can be also gainfully read by those who wish to apply the methods in another software. 

Feedback is appreciated!

Please either start an issue here:https://gitlab.com/oliver.nakoinz/klassifikator/-/issues
or write an e-mail to s.c.schmidt@fu-berlin.de

This tutorial was published on zenodo under the [DOI 10.5281/zenodo.6325372](https://doi.org/10.5281/zenodo.6325372) and can be cited as:  

Schmidt, Sophie C., Martini, Sarah, Staniuk, Robert, Quatrelivre, Carole, Hinz, Martin, Nakoinz, Oliver, Bilger, Michael, Roth, Georg, & Laabs, Julian. (2022, March 3). Tutorial on Classification in Archaeology: Distance Matrices, Clustering Methods and Validation. Zenodo. https://doi.org/10.5281/zenodo.6325372


## Meta data

- dates: 
    - winter schook: 09.03.2020 - 12.03.2020
    - release first version: 2020-03-03

- place: 
    - Christian-Albrechts-Universität zu Kiel

- languages: 
    - course: English, German
    - tutorial: English
    - Scripting language: R
    
- people: 
    - organisation: Oliver Nakoinz (Kiel)
    - Guest docents: 
        - Sophie Schmidt (Köln)
        - Martin Hinz (Bern)
        - Georg Roth (Berlin)
        
- Institutions: 
    - [ISAAK](https://isaakiel.github.io)
    - [Johanna Mestorf Academy](http://www.jma.uni-kiel.de/en) 
    - [SFB 1266/A2](http://www.sfb1266.uni-kiel.de/de)
- funding: [Perle](http://www.perle.uni-kiel.de/de)
- link to the page: [https://gitlab.com/oliver.nakoinz/klassifikator](https://gitlab.com/oliver.nakoinz/klassifikator)
- contact: mod@gshdl.uni-kiel.de; s.c.schmidt@fu-berlin.de
- [Flyer](./4figures/Flyerentwurf8.pdf)
- published unter [DOI 10.5281/zenodo.6325372](https://doi.org/10.5281/zenodo.6325372) 
